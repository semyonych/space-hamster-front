import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import "./Profile.css";
import ProfileGeneral from "./ProfileGeneral/ProfileGeneral";


function Profile() {

    let { path } = useRouteMatch();

    return (
        <Container>
            <Row>
                <Col>
                    <Switch>
                        <Route exact path={path}>
                            <ProfileGeneral/>
                        </Route>
                    </Switch>
                </Col>
            </Row>
        </Container>
    )
}

export default Profile;