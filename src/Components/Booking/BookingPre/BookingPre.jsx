import React from "react";
import { Link } from 'react-router-dom';

function BookingPre(props) {
    function getDate() {
        props.setCurrentDate(new Date());
    }
    return (
        <>
            <Link to='/booking/room' className='booking-pre-button' onClick={getDate}>
               Lets book your visit!
            </Link>
        </>
    )
}

export default BookingPre;