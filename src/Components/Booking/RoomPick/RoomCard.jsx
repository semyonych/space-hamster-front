import React from "react";
import { Card, Form } from 'react-bootstrap';

function RoomCard(props) {
    const room = props.room;
    const schedules = Object.entries(room.schedules);
    
    return (
        <Card className='single-room-card'>
            <Card.Body>
            <Card.Title>{room.name}</Card.Title>
            
                {schedules.map(([key, value]) => {
                    return (
                        <div className='d-block' key={key}>
                            <div>{key}</div>
                            {/* <div>{value.guestReservable}</div> */}
                            <div>{value.from} - {value.to}</div>
                        </div>       
                    )
                })}
                <Form.Check
                    type="radio"
                    name="roomradio"
                    value={props.value}
                    onClick={props.pickRoom}
                />
            </Card.Body>
        </Card>
    )
}

export default RoomCard;