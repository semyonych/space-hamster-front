import React, { useState } from "react";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import RoomCard from "./RoomCard";

function RoomPick(props) {

  const [roomPickedStatus, setRoomPickedStatus] = useState(false);

  const pickRoom = (event) => {
    setRoomPickedStatus(true);
    props.setPickedRoom(event.target.value);
  }
  return (
    <>
      <h3 className='form-room-title'>Pick room you want to visit!</h3>
        <Form className='form__room justify-content-around d-flex' >
        {props.rooms.map(room => (
          <RoomCard key={room.id} room={room} value={room.id} pickRoom={pickRoom} />
        ))}
        </Form> 
      { (roomPickedStatus) ? (
          <Link to="/booking/day">
            <Button>Next</Button>
          </Link>
        ) : (
          <Button disabled>Next</Button>
        )}
    </>
  );
}

export default RoomPick;
