import React, { useState } from "react";
import { Form, Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";

function RoomPick(props) {

  const [roomPickedStatus, setRoomPickedStatus] = useState(false);

  const pickRoom = (event) => {
    setRoomPickedStatus(true);
  }
  return (
    <>
      {props.rooms.map(room => (
        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src="holder.js/100px180" />
          <Card.Body>
            <Card.Title>Card Title</Card.Title>
            <Card.Text>
              Some quick example text to build on the card title and make up the bulk of
              the card's content.
            </Card.Text>
            <Button variant="primary">Go somewhere</Button>
          </Card.Body>
        </Card>
      ))}
      
    
    // <>
    //   <Form className='form__room d-flex justify-content-around'>
    //     {props.rooms.map((room) => (
    //       <Form.Check
    //         key={room.id}
    //         type="radio"
    //         name="roomradio"
    //         value={room.id}
    //         label={room.name}
    //         onClick={pickRoom}
    //       />
    //     ))}  
    //   </Form>
    //   { (roomPickedStatus) ? (
    //     <Link to="/booking/day">
    //       <Button>Next</Button>
    //     </Link>
    //   ) : (
    //     <Button disabled>Next</Button>
    //   )}
    // </>
  );
}

export default RoomPick;
