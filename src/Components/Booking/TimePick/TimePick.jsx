import React, { useState } from "react";
import { Link } from 'react-router-dom';
import { Button, Form } from 'react-bootstrap'; 
import ScheduleCard from "./ScheduleCard";

function TimePick(props) {

    const [timePickedStatus, setTimePickedStatus] = useState(false);

    const pickTime = (event) => {
        props.setPickedSchedule(event.target.value);
        setTimePickedStatus(true);
    }

    function checkForId (room) {
        if (room.id === props.pickedRoom) {
            return room;
        }
    }

    const pickedRoomObject = props.rooms.find(checkForId);

    const schedules = Object.entries(pickedRoomObject.schedules);

    return (
        <>
            <Form className='form__time'>
                {schedules.map(([key, value]) => (
                    <ScheduleCard key={key} name={key} from={value.from} to={value.to} capacity={value.capacity} reservable={value.guestReservable} pickTime={pickTime} />
                ))}  
            </Form>
            <Link to='/booking/day'>
                <Button>Prev</Button>
            </Link>
            {(timePickedStatus) ? (
                <Link to='/booking/pre-send-check'>
                    <Button>Check order</Button>
                </Link>
            ) : (
                <Button disabled>Check order</Button>
            )}
            
        </>
    )
}

export default TimePick;