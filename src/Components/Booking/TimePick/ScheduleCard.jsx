import React from "react";
import { Card, Form } from 'react-bootstrap';

function ScheduleCard(props) {
    return(
        <Card className='schedule-card'>
            <Card.Body className='d-flex align-items-center justify-content-between schedule-card__card-content'>
            <Card.Title className='card-content__title'>{props.name}</Card.Title>
                <div className='d-flex card-content__info'>
                    <div className='info__time'>{props.from} - {props.to}</div>
                    <div className='info__capacity'>Room capacity: {props.capacity}</div>
                </div>
            
                <Form.Check
                    disabled={!props.reservable}
                    type="radio"
                    name="schedule"
                    value={props.name}
                    onClick={props.pickTime}
                />
            </Card.Body>
        </Card>
    )
    
}

export default ScheduleCard;