import React, { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import {
    Switch,
    Route,
    useRouteMatch
  } from "react-router-dom";
import './Booking.css'
import RoomPick from "./RoomPick/RoomPick";
import DayPick from "./DayPick/DayPick";
import TimePick from "./TimePick/TimePick";
import CheckOrder from "./CheckOrder/CheckOrder";
import OrderSuccess from "./OrderSuccess/OrderSuccess";
import BookingPre from "./BookingPre/BookingPre";

function Booking(props) {
    
    let { path } = useRouteMatch();

    const [rooms, setRooms] = useState();

    const [pickedRoom, setPickedRoom] = useState();

    const [pickedSchedule, setPickedSchedule] = useState();

    const [currentDate, setCurrentDate] = useState();

    const [pickedDay, setPickedDay] = useState();

    useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var graphql = JSON.stringify({
            query: "{\r\n    getRooms(\r\n        tenantId: \"" + props.tenantId + "\"\r\n    ) { \r\n        id\r\n        name\r\n        capacity\r\n        schedules\r\n    }\r\n}",
            variables: {}
        })
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: graphql,
            redirect: 'follow'
    };

    fetch(props.url, requestOptions)
    .then(response => response.json())
    .then(result => setRooms(result.data.getRooms))
    .catch(error => console.log('error', error));

    }, [props.tenantId, props.url]);

    return (
        <Container>
            <Row>
                <Col className='booking-wrapper'>
                    <Switch>
                        <Route exact path={`${path}`}>
                            <BookingPre setCurrentDate={setCurrentDate}/>
                        </Route>
                        <Route path={`${path}/room`}>
                            <RoomPick 
                                rooms={rooms} 
                                setPickedRoom={setPickedRoom} 
                            />
                        </Route>
                        <Route path={`${path}/day`}>
                            <DayPick 
                                rooms={rooms}
                                currentDate={currentDate} 
                                setPickedDay={setPickedDay} 
                                pickedRoom={pickedRoom}
                                url={props.url}
                            />
                        </Route>
                        <Route path={`${path}/time`}>
                            <TimePick 
                                pickedRoom={pickedRoom} 
                                rooms={rooms} 
                                setPickedSchedule={setPickedSchedule} 
                            />
                        </Route>
                        <Route path={`${path}/pre-send-check`}>
                            <CheckOrder 
                                pickedSchedule={pickedSchedule} 
                                pickedRoom={pickedRoom} 
                                pickedDay={pickedDay} 
                                rooms={rooms} 
                                guestId={props.guestId}
                                url={props.url}
                            />
                        </Route>
                        <Route path={`${path}/success`}>
                            <OrderSuccess 
                                guestId={props.guestId} 
                                rooms={rooms}
                                url={props.url}
                            />
                        </Route>
                    </Switch>
                </Col>
            </Row>
        </Container>
        
    )
}

export default Booking;