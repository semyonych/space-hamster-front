import React from "react";
import { Link, useHistory } from 'react-router-dom';
import { Button } from 'react-bootstrap'; 

function CheckOrder(props) {
    const pickedRoom = props.pickedRoom;

    const pickedDay = new Date(props.pickedDay);

    const pickedSchedule = props.pickedSchedule;
    
    const guestId = props.guestId;

    const date = pickedDay.getUTCDate();

    const month = pickedDay.getUTCMonth();

    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    function getRoomName(room) {
        if (room.id === pickedRoom) {
            return room;
        }
    }

    const roomName = props.rooms.find(getRoomName);

    const history = useHistory();

    function handleReservationSubmit() {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var graphql = JSON.stringify({
        query: "mutation {\r\n    reserveRoom(\r\n        roomId: " + roomName.id + ",\r\n        time: \"" + pickedSchedule + "\",\r\n        day: \"" + pickedDay + "\"\r\n        guestId: " + guestId + "\r\n    ) \r\n}",
        variables: {}
        })
        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: graphql,
        redirect: 'follow'
        };

        fetch(props.url, requestOptions)
        .then(response => response.json())
        .then(result => {
            if (result.errors) {
                console.log(result.errors);
            } else {
                history.push('/booking/success');
            }
        })
        .catch(error => console.log('error', error));
    }
    
    return (
        <>
            <div className='check-order__information'>
                <h1 className='information__title'>
                    Your order:
                </h1>
                <div className='information__info-line'>
                    <div className='info-line__title'>
                        Room:
                    </div>
                    <div className='info-line__value'>
                        {roomName.name}
                    </div>
                </div>
                <div className='information__info-line'>
                    <div className='info-line__title'>
                        Day:
                    </div>
                    <div className='info-line__value'>
                        {date} {monthNames[month]}
                    </div>
                </div>
                <div className='information__info-line'>
                    <div className='info-line__title'>
                        Schedule:
                    </div>
                    <div className='info-line__value'>
                        {pickedSchedule}
                    </div>
                </div>
            </div>
            <Link to='/booking/time'>
                <Button>Prev</Button>
            </Link>
            {/* <Link to='/booking/success' >
                
            </Link> */}
            <Button onClick={handleReservationSubmit}>Proceed to payment</Button>
            
        </>
    )
}

export default CheckOrder;