import React, { useState, useEffect } from "react";
import { Button, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function OrderSuccess(props) {

    const [userReservations, setUserReservations] = useState([]);
    const [qrCode, setQrCode] = useState(null);

    useEffect(() => {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var graphql = JSON.stringify({
        query: "{\r\n    ownReservations(\r\n        guestId: "+ props.guestId +"\r\n    ) { \r\n        id\r\n        roomId\r\n        time\r\n    }\r\n}",
        variables: {}
        })
        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: graphql,
        redirect: 'follow'
        };

        fetch(props.url, requestOptions)
        .then(response => response.json())
        .then(result => setUserReservations(result.data.ownReservations))
        .catch(error => console.log('error', error));

    }, [props.guestId, props.url]);

    const [reservationIds, setUserReservationIds] = useState([]);

    function pushIdIntoQueryArray(event) {
        if (event.target.checked === true) {
            setUserReservationIds([...reservationIds, event.target.value]);
        } else {
            const newUserReservations = reservationIds;
            for( let i = 0; i < newUserReservations.length; i++){ 
    
                if ( newUserReservations[i] === event.target.value) { 
            
                    newUserReservations.splice(i, 1); 
                }
            
            }
            setUserReservationIds(newUserReservations);
        }
    }

    function GenerateQR() {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var graphql = JSON.stringify({
        query: "mutation {\r\n    generateQR(\r\n        reservationIds: ["+ reservationIds +"],\r\n        guestId: " + props.guestId + "\r\n    )\r\n}",
        variables: {}
        });
        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: graphql,
        redirect: 'follow'
        };

        fetch(props.url, requestOptions)
        .then(response => response.json())
        .then(result => {
            if ('errors' in result) {
                alert('No reservations selected. Please select at least one reservation first.')
            } else {setQrCode(result.data.generateQR); }}
        )
        .catch(error => console.log('error', error));
    }

    function getRoomName(room, pickedRoom) {
        if (room.id === pickedRoom) {
            return room;
        }
    }
    
    return (
        <>
            <h1>Thank you!</h1>
            <p>You have successfully booked your order.</p>
            <p>You can book another order or generate QR-code for your existing orders.</p>
            <p>To generate QR pick reservations you want in list below and then press "Generate QR" button.</p>
            <p>QR code will be sent to your email.</p>
            <div className='d-flex justify-content-center'>
                <Link to='/booking'>
                    <Button>Booking</Button>
                </Link>
                <Button
                onClick={GenerateQR}
                >
                    Generate QR
                </Button>
            </div>
            {qrCode !== null && (
                <div>
                    <h2>Your QR-code:</h2>
                    <img src={qrCode} alt="qr-code"/>
                </div>
            )
            
            }

            {userReservations.length > 0 
                && 
                (
                    <>
                        <h4 className='order-succes-reservations-title'>Your reservations:</h4>
                        <ul className='order-success-reservations-list'>
                            {userReservations.map(
                                (reservation, index) => 
                                <li key={index} className='d-flex list-single-item'>
                                    <Form.Check
                                        type='checkbox'
                                        value={reservation.id}
                                        onChange={pushIdIntoQueryArray}
                                        className='list-checkbox'
                                    />
                                    <div className='list-data'><strong>ID</strong>: {reservation.id}</div>
                                    <div className='list-data'><strong>Room</strong>: {props.rooms.find((room) => getRoomName(room, reservation.roomId)).name}</div>
                                    <div className='list-data'><strong>Schedule</strong>: {reservation.time} </div>
                                </li>
                                )
                            }
                        </ul>
                    </>
                )
            } 
        </>
    )
}

export default OrderSuccess;