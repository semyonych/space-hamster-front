import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import { Button, Form } from 'react-bootstrap';
import DayCard from "./DayCard";
 
function DayPick(props) {

    const [dayPickedStatus, setDayPickedStatus] = useState(false);

    const roomCapacity = props.rooms.find(room => getCapacity(room, props.pickedRoom)).capacity;

    function getCapacity(room, pickedRoom) {
        if (room.id === pickedRoom) {
            return room;
        }
    }
    

    const pickDay = (event) => {
        props.setPickedDay(event.target.value);
        setDayPickedStatus(true);
    };

    const [week, setWeek] = useState([]);

    const dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    useEffect(()=> { 
        let day = new Date();
        let days = [];
        for (let i = 0; i < 8; i++ ) {
            days.push(new Date(day));
            day.setDate(day.getDate() + 1); 
            // console.log(day);
        }
        setWeek(days);
    }, []);

    

    return (
        <>  
            <Form className='form__day'>
                {week.map((day, index) => (
                    <DayCard 
                        day={day} 
                        key={index} 
                        dayNames={dayNames} 
                        pickedRoom={props.pickedRoom}
                        roomCapacity={roomCapacity}
                        pickDay={pickDay}
                        url={props.url}
                    />
                ))}  
            </Form>
            <Link to='/booking/room'>
                <Button>Prev</Button>
            </Link>
            {(dayPickedStatus) ? (
                <Link to='/booking/time'>
                    <Button>Next</Button>
                </Link>
            ) : (
                <Button disabled>Next</Button>
            )}
            
        </>
    )
}

export default DayPick;