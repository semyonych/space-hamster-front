import React, { useState, useEffect } from "react";
import { Card, Form } from 'react-bootstrap';

function DayCard(props) {

    const [roomAggregates, setRoomAggregates] = useState();
    const [dayData ,setDayData] = useState();

    useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var graphql = JSON.stringify({
        query: "{\r\n    getRoomAggregates(\r\n        roomId: "+ props.pickedRoom +"\r\n    ) { \r\n        id\r\n        day\r\n        reservations\r\n    }\r\n}",
        variables: {}
        })
        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: graphql,
        redirect: 'follow'
        };

        fetch(props.url, requestOptions)
        .then(response => response.json())
        .then(result => setRoomAggregates(result.data.getRoomAggregates))
        .catch(error => console.log('error', error));

    }, [props.pickedRoom, props.url]);


    const date = new Date(props.day);

    const numericalMonth = date.getMonth();

    const numericalDate = date.getDate();

    function getDayData(reservation, numericalDate, numericalMonth) {
        let reservationDate = new Date(reservation.day).getDate();
        // console.log(reservationDate);
        let reservationMonth = new Date(reservation.day).getMonth();
        // console.log(reservationMonth);
        // console.log(numericalDate, numericalMonth);
        if (numericalDate === reservationDate && numericalMonth === reservationMonth) {
            return reservation;
        } 
    }

    useEffect(() => {
        if (Array.isArray(roomAggregates) === true) {
            const singleDayData = roomAggregates.find(reservation => getDayData(reservation, numericalDate, numericalMonth));
            setDayData(singleDayData);
        }
        
    }, [dayData, roomAggregates, numericalDate, numericalMonth]);

    // console.log(dayData);

    return (
        <Card className='single-day-card'>
            <Card.Body>
            <Card.Title>{props.day.getUTCDate().toString() + ' ' + props.dayNames[props.day.getUTCDay()] }</Card.Title>
                {dayData === undefined 
                    ? 
                    (<div>
                        No reservation on this day.
                    </div>) 
                    : 
                    (
                        Object.entries(dayData.reservations).map(([key, value]) => ( 
                                <div key={key}>
                                    {typeof value === 'object' && value !== null && value
                                        ? 
                                        (
                                            Object.entries(value).map(
                                                ([key, value]) => (
                                                    <div key={key} className='d-flex justify-content-center'>
                                                        <span>{key}:</span>
                                                        {' '} 
                                                        <span>{value}/{props.roomCapacity}</span>
                                                    </div>
                                                )
                                            )
                                        )
                                        :
                                        (
                                            <></>
                                            // <div className='d-flex justify-content-center'>
                                            //     <span>{key}:</span>
                                            //     {' '} 
                                            //     <span>{value}/{props.roomCapacity}</span>
                                            // </div>
                                        )
                                    }
                                </div>
                            )
                        )
                    )
                }
                <Form.Check
                    type="radio"
                    name="dayradio"
                    // label={props.day.getUTCDate().toString() + ' ' + props.dayNames[props.day.getUTCDay()] }
                    value={props.day}
                    onClick={props.pickDay}
                />
            </Card.Body>
        </Card>
    )
}

export default DayCard;