
import './App.css';
import {useState} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
// import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Booking from "../Booking/Booking";
import Profile from "../Profile/Profile";
import LoginAuth from '../LoginAuth/LoginAuth';
import NavigationRow from '../NavigationRow/NavigationRow';
import ProfileCreation from '../ProfileCreation/ProfileCreation';
import LoginVerify from '../LoginVerify/LoginVerify';

function App() {

  // const [url, setUrl] = useState('https://spacehamster.ch/graphql');

  const [loggedIn, setLoggedIn] = useState(false);

  const tenantId = 'd0772378-7c87-44f1-aa2a-0cc1c045a821';

  const [guestId, setGuestId] = useState();

  const changeLogStatus = () => {
    setLoggedIn(true);
  };

  const url = 'https://spacehamster.ch/graphql';

  // useEffect(() => {
  //   const hostname = window.location.hostname;
  //
  //   if (hostname === 'localhost') {
  //     setUrl('https://spacehamster.ch/graphql');
  //   } else {
  //     setUrl('/graphql');
  //   }
  // }, []);


  return (
    <div className="App">
      <Router>
        <NavigationRow loggedIn={loggedIn} />
        <Switch>
          <Route exact path='/'>
            <LoginAuth changeLogStatus={changeLogStatus} setGuestId={setGuestId} url={url}  />
          </Route>
          <Route path='/booking'>
            <Booking tenantId={tenantId} guestId={guestId} url={url} />
          </Route>
          <Route path='/profile'>
            <Profile />
          </Route>
          <Route path='/profile-creation'>
            <ProfileCreation tenantID={tenantId} />
          </Route>
          <Route path='/verify'>
            <LoginVerify changeLogStatus={changeLogStatus} guestId={guestId} url={url} />
          </Route>
          
        </Switch>
      </Router>
    </div>
  );
}

export default App;
