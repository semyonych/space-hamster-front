import React, { useState } from "react";
import { Container, Row, Col, Button, Form } from "react-bootstrap";
import { useHistory } from "react-router";
import './LoginVerify.css';

function LoginVerify(props) {

    const [codeValue, setCodeValue] = useState('');
    // const [idValue, setIdValue] = useState('');

    const history = useHistory();

    const setCode = (event) => {
        setCodeValue(event.target.value);
    };

    const verifySuccess = () => {
        history.push('/booking');
        props.changeLogStatus();
    }

    const submitVerificationCode = () => {
        let code = codeValue.toString();
        // let id = idValue.toString();
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var graphql = JSON.stringify({
        query: "mutation {\r\n    guestVerify(\r\n        guestId: "+ props.guestId +",\r\n        code: \"" + code +"\"\r\n    ) \r\n}",
        variables: {}
        })
        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: graphql,
        redirect: 'follow'
        };

        fetch(props.url, requestOptions)
        .then(response => response.json())
        .then(result => {
            result.data.guestVerify === true ? (verifySuccess()) : (console.log(result))
        })
        .catch(error => console.log('error', error));

    };

    return (
        <Container>
            <Row>
                <Col className='d-flex justify-content-center'>
                    <Form 
                        className='form-body d-flex flex-column'
                        
                    >
                        <h3 className='form-body__title'>
                            Verify you email.
                        </h3>
                        <p className='form-body__text'>Please check you mailbox for mail with verification code and enter it in form below.</p>
                        <Form.Label htmlFor="inlineFormInputName" className='form-body__label'>
                            Enter you verification code.
                        </Form.Label>
                        <Form.Control placeholder="Verification code" className='form-body__input' onChange={setCode} />
                        {/* <Link className='form-body__create-link' to='/profile-creation'>Create account</Link> */}
                        <div className='button-row d-flex justify-content-end'>
                            {/* <Link className='dummy-link' to='/booking/room'> */}
                                <Button className='form-body__submit-btn' onClick={submitVerificationCode}>Proceed</Button>
                            {/* </Link> */}
                        </div>
                        
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default LoginVerify;