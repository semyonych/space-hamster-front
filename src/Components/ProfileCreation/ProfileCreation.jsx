import React from "react";
import { Button, Container, Row, Col, Form } from 'react-bootstrap';
import './ProfileCreation.css';

function ProfileCreation() {
    return (
        <Container>
            <Row>
                <Col className='d-flex justify-content-center'>
                    <Form className='creation-form-body d-flex flex-column'>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Full name</Form.Label>
                            <Form.Control type="text" placeholder="Enter full name" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" className='creatioin-form-body__checkbox' label="I agree to terms and conditions" />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Create account
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
                
    )
}

export default ProfileCreation;