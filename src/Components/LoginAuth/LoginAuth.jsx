import React, { useState } from "react";
import './LoginAuth.css';
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

function LoginAuth(props) {
    const history = useHistory();
    
    const [email, setEmail] = useState('');

    const setEmailValue = (event) => {
        setEmail(event.target.value);
    };

    const submitEmail = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        let emailValue = email.toString();

        var graphql = JSON.stringify({
        query: "mutation {\r\n    guestRegister(\r\n        email: \"" + emailValue +"\",\r\n        tenantId: \"d0772378-7c87-44f1-aa2a-0cc1c045a821\"\r\n    ) { \r\n        id\r\n        email\r\n        code\r\n        verified\r\n    }\r\n}",
        variables: {}
        })
        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: graphql,
        redirect: 'follow'
        };

        fetch(props.url, requestOptions)
        .then(response => response.json())
        .then(result => 
            {
                console.log(result.data.guestRegister.code);
                history.push('/verify');
                props.setGuestId(result.data.guestRegister.id);
            })
        .catch(error => console.log('error', error));
    };

    

    return (
        <Container>
            <Row>
                <Col className='d-flex justify-content-center'>
                    <Form 
                        className='form-body d-flex flex-column'
                        onChange={setEmailValue}
                    >
                        <h3 className='form-body__title'>
                            Enter your email.
                        </h3>
                        <Form.Label htmlFor="inlineFormInputName" className='form-body__label'>
                            Email
                        </Form.Label>
                        <Form.Control placeholder="Enter you email" className='form-body__input' />
                        {/* <Link className='form-body__create-link' to='/profile-creation'>Create account</Link> */}
                        <div className='button-row d-flex justify-content-end'>
                            {/* <Link className='dummy-link' to='/booking/room'> */}
                                <Button className='form-body__submit-btn' onClick={submitEmail}>Proceed</Button>
                            {/* </Link> */}
                        </div>
                        
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default LoginAuth;