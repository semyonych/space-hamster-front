import React from "react";
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './NavigationRow.css'

function NavigationRow(props) {

    
    return (
        <Container>
            <Row>
                <Col className='navigation-menu'>
                    {
                    (props.loggedIn === true) ? 
                        <>
                            <Link className='navigation-menu__link' to='/booking'>
                                <Button className='link__button'>
                                    Booking
                                </Button>  
                            </Link> 
                            <Link className='navigation-menu__link' to='/profile'>
                                <Button className='link__button'>
                                    Profile
                                </Button> 
                            </Link>
                        </> 
                        : 
                        <>
                            <Link className='navigation-menu__link' to='/'>
                                <Button className='link__button'>
                                    Booking
                                </Button>
                            </Link>
                            {/*<Link className='navigation-menu__link' to='/profile-creation'>*/}
                            {/*    */}
                            {/*</Link>*/}
                            <Button className='link__button' disabled>
                                Profile
                            </Button>
                        </>
                    }
                </Col>
            </Row>
        </Container>
    )
}

export default NavigationRow;